IDENTIFICATION DIVISION.
PROGRAM-ID. exFicSEQbis.
ENVIRONMENT DIVISION.
CONFIGURATION SECTION.
SPECIAL-NAMES.
DECIMAL-POINT IS COMMA.
INPUT-OUTPUT SECTION.

FILE-CONTROL.

*> Association des fichiers séquentiels

SELECT fdocteurs ASSIGN TO "docteurs.dat" 
ORGANIZATION sequential 
ACCESS IS sequential
FILE STATUS IS fdocteurs_CR.

SELECT fmaladies ASSIGN TO "maladies.dat" 
ORGANIZATION sequential 
ACCESS IS sequential
FILE STATUS IS fmaladies_CR.

*> Association des fichiers indexés

SELECT fpatients ASSIGN TO "patients.dat" 
ORGANIZATION indexed 
ACCESS IS DYNAMIC
RECORD KEY fpat_id
ALTERNATE RECORD KEY fpat_maladieNum WITH DUPLICATES
ALTERNATE RECORD KEY fpat_batimentNum WITH DUPLICATES
FILE STATUS IS fpatients_CR.

SELECT fbatiments ASSIGN TO "batiments.dat" 
ORGANIZATION indexed 
ACCESS IS DYNAMIC
RECORD KEY fbat_num
ALTERNATE RECORD KEY fbat_responsable
FILE STATUS IS fbatiments_CR.

DATA DIVISION. 

*> Declaration des attributs

FILE SECTION.

FD fdocteurs.
01 docteur_tamp.
02 fdoc_id PIC 9(6).
02 fdoc_nom PIC A(30).
02 fdoc_prenom PIC A(30).
02 fdoc_secteur PIC A(30).
02 fdoc_rangResp PIC 9(2).

FD fmaladies.
01 maladie_tamp.
02 fmal_num PIC 9(6).
02 fmal_nom PIC A(30).
02 fmal_tauxM PIC 9(3)V99.
02 fmal_tauxT PIC 9(3)V99.

FD fpatients.
01 patient_tamp.
02 fpat_id PIC 9(12).
02 fpat_nom PIC A(30).
02 fpat_prenom PIC A(30).
02 fpat_docteurNum PIC 9(6).
02 fpat_batimentNum PIC 9(4).
02 fpat_chambre PIC 9(4).
02 fpat_maladieNum PIC 9(6).
02 fpat_niveau PIC A(30).

FD fbatiments.
01 batiment_tamp.
02 fbat_num PIC 9(4).
02 fbat_responsable PIC 9(6).
02 fbat_nbPlacesLibres PIC 9(5).
02 fbat_capacite PIC 9(5).
02 fbat_maladieNum PIC 9(6).
02 fbat_droitAcces PIC 9(2).


WORKING-STORAGE SECTION.
77 fdocteurs_CR PIC X(2). 
77 fmaladies_CR PIC X(2). 
77 fpatients_CR PIC X(2). 
77 fbatiments_CR PIC X(2).
77 WMenu PIC 9.
77 Wrep PIC 9.

PROCEDURE DIVISION.
PERFORM MENU

STOP RUN.

MENU.
PERFORM WITH TEST AFTER UNTIL Wmenu = 0
    DISPLAY '1 - Ajouter 2 - Afficher 3 - Supprimer 4 - Modifier 0- Quitter'
    ACCEPT Wmenu 

    IF Wmenu = 1 THEN
        PERFORM MENU_AJOUTER
        ELSE IF Wmenu = 2 THEN
            PERFORM MENU_AFFICHER
            ELSE IF Wmenu = 3 THEN
                PERFORM MENU_SUPPRIMER
                ELSE IF Wmenu = 4 THEN
                    PERFORM MENU_MODIFIER
                END-IF
            END-IF
        END-IF
    END-IF
END-PERFORM.

MENU_AJOUTER.
MOVE 0 TO wMenu

    DISPLAY '1 - Ajouter un patient 2 - Ajouter un docteur 3 - Ajouter un batiment 4 - Ajouter une maladie 0- Quitter'
    ACCEPT Wmenu 

    IF Wmenu = 1 THEN
        PERFORM AJOUTER_PATIENT
        ELSE IF Wmenu = 2 THEN
            PERFORM AJOUTER_DOCTEUR
            ELSE IF Wmenu = 3 THEN
                PERFORM AJOUTER_BATIMENT
                ELSE IF Wmenu = 4 THEN
                    PERFORM AJOUTER_MALADIE
                    ELSE IF Wmenu = 0 THEN
                       PERFORM MENU
                    END-IF
                END-IF
            END-IF
        END-IF
    END-IF.

MENU_AFFICHER.

    DISPLAY '1 - Afficher patients 2 - Afficher docteurs 3 - Afficher batiments 4 - Afficher maladies 0- Quitter'
    ACCEPT Wmenu 

    IF Wmenu = 1 THEN
        PERFORM AFFICHER_PATIENTS
        ELSE IF Wmenu = 2 THEN
            PERFORM AFFICHER_DOCTEURS
            ELSE IF Wmenu = 3 THEN
                PERFORM AFFICHER_BATIMENTS
                ELSE IF Wmenu = 4 THEN
                    PERFORM AFFICHER_MALADIES
                    ELSE IF Wmenu = 0 THEN
                       PERFORM MENU
                    END-IF
                END-IF
            END-IF
        END-IF
    END-IF.

MENU_SUPPRIMER.
    DISPLAY '1 - Supprimer patient 2 - Supprimer docteur 3 - Supprimer batiment 4 - Supprimer maladie 0- Quitter'
    ACCEPT Wmenu 

    IF Wmenu = 1 THEN
        PERFORM SUPPRIMER_PATIENT
        ELSE IF Wmenu = 2 THEN
            PERFORM SUPPRIMER_DOCTEUR
            ELSE IF Wmenu = 3 THEN
                PERFORM SUPPRIMER_BATIMENT
                ELSE IF Wmenu = 4 THEN
                    PERFORM SUPPRIMER_MALADIE
                    ELSE IF Wmenu = 0 THEN
                       PERFORM MENU
                    END-IF
                END-IF
            END-IF
        END-IF
    END-IF.

MENU_MODIFIER.
    DISPLAY '1 - Modifier patient 2 - Modifier docteur 3 - Modifier batiment 4 - Modifier maladie 0- Quitter'
    ACCEPT Wmenu 

    IF Wmenu = 1 THEN
        PERFORM MODIFIER_PATIENT
        ELSE IF Wmenu = 2 THEN
            PERFORM MODIFIER_DOCTEUR
            ELSE IF Wmenu = 3 THEN
                PERFORM MODIFIER_BATIMENT
                ELSE IF Wmenu = 4 THEN
                    PERFORM MODIFIER_MALADIE
                    ELSE IF Wmenu = 0 THEN
                       PERFORM MENU
                    END-IF
                END-IF
            END-IF
        END-IF
    END-IF.


AJOUTER_PATIENT.

AJOUTER_DOCTEUR.

AJOUTER_BATIMENT.

AJOUTER_MALADIE.
OPEN EXTEND fmaladies
IF fmaladies_CR = 35 THEN
       OPEN OUTPUT fmaladies
END-IF

PERFORM WITH TEST AFTER UNTIL Wrep = 0
       DISPLAY 'Donnez les informations relatives à la maladie'
       DISPLAY 'Numéro de la maladie :'
       ACCEPT fmal_num
       DISPLAY 'Nom de la maladie : '
       ACCEPT fmal_nom
       DISPLAY 'Taux de mortalité (en %) : '
       ACCEPT fmal_tauxM
       DISPLAY 'Taux de transmission :'
       ACCEPT fmal_tauxT
       WRITE maladie_tamp END-WRITE
       PERFORM WITH TEST AFTER UNTIL Wrep = 0 OR Wrep = 1
           DISPLAY 'Voulez vous ajouter une autre maladie ? 1 ou 0'
           ACCEPT Wrep
           CLOSE fmaladies
       END-PERFORM
END-PERFORM.

AFFICHER_PATIENTS.

AFFICHER_DOCTEURS.

AFFICHER_BATIMENTS.

AFFICHER_MALADIES.

SUPPRIMER_PATIENT.

SUPPRIMER_DOCTEUR.

SUPPRIMER_BATIMENT.

SUPPRIMER_MALADIE.

MODIFIER_PATIENT.

MODIFIER_DOCTEUR.

MODIFIER_BATIMENT.

MODIFIER_MALADIE.




